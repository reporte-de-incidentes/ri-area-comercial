package com.RIareacomercial.controller;

import com.RIareacomercial.service.ClienteService;
import com.RIcommons.dto.ClienteDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clientes")
@Tag(name = "Clientes", description = "Servicio para realizar operaciones relacionadas a los clientes.")
@CrossOrigin
public class ClienteController
{
    @Autowired
    ClienteService clienteService;

    @GetMapping("/allClientes")
    @Operation(summary = "Obtiene todos los clientes")
    public ResponseEntity<List<ClienteDTO>> getClientes(){
        return new ResponseEntity<>(clienteService.obtenerClientes(), HttpStatus.OK);
    }

    @PostMapping("/save")
    @Operation(summary = "Guarda un nuevo cliente.")
    public ResponseEntity<String> saveCliente(@RequestBody ClienteDTO clienteDTO){
        clienteService.guardarCliente(clienteDTO);
        return ResponseEntity.ok("El cliente ha sido cargado con exito");
    }

    @PutMapping("/modify/{id_cliente}")
    @Operation(summary = "Modifica los datos de un cliente.")
    public ResponseEntity<String> modifyCliente(@PathVariable(value = "id_cliente", required = true) int id_cliente,
                                                @RequestBody ClienteDTO clienteDTO) throws Exception {

        clienteService.modificarCliente(clienteDTO);
        return ResponseEntity.ok("El cliente ha sido modificado con exito");
    }

    @DeleteMapping("/delete/{id_cliente}")
    @Operation(summary = "Elimina un cliente mediante el id.")
    public ResponseEntity<String> deleteCliente(@PathVariable(value = "id_cliente", required = true) int id_cliente){
        clienteService.eliminarCliente(id_cliente);
        return ResponseEntity.ok("El cliente ha sido dado de baja con exito");
    }
}
