package com.RIareacomercial.service;

import com.RIareacomercial.repository.IClienteRepository;
import com.RIcommons.assembler.DTOsAssembler;
import com.RIcommons.assembler.EntityAssembler;
import com.RIcommons.dto.ClienteDTO;
import com.RIcommons.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService
{
    @Autowired
    IClienteRepository clienteRepository;
    @Autowired
    EntityAssembler entityAssembler;
    @Autowired
    DTOsAssembler dtOsAssembler;

    public List<ClienteDTO> obtenerClientes(){
        List<Cliente> clienteList = clienteRepository.findAll();
        List<ClienteDTO> clienteDTOList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(clienteList)){
            for (Cliente cliente : clienteList){
                clienteDTOList.add(dtOsAssembler.toClienteDTO(cliente));
            }
        }
        return clienteDTOList;
    }


    public void guardarCliente(ClienteDTO clienteDTO){
        Cliente cliente = entityAssembler.toCliente(clienteDTO);
        clienteRepository.save(cliente);
    }

    public void modificarCliente(ClienteDTO clienteDTO) throws Exception {
        Cliente cliente = clienteRepository.findById(clienteDTO.getNro_cliente()).orElseThrow(()
                -> new Exception("El cliente nro " + clienteDTO.getNro_cliente() + " no existe"));
        cliente.setNombre(clienteDTO.getNombre());
        cliente.setDni(clienteDTO.getDni());
        cliente.setEmail(clienteDTO.getEmail());
        cliente.setNro_contacto(clienteDTO.getNro_cliente());

        clienteRepository.save(cliente);
    }

    public void eliminarCliente(int id_cliente){
        clienteRepository.deleteById(id_cliente);
    }
}
